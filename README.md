# IPFS-tiny

This project is dedicated to the research and architectural challenges regarding an implementation of IPFS with respect to os-independence and resources restriction.

## Research Tracking

Any steps taken for the investigational part of this project, are tracked in the Wiki Pages of the repository.