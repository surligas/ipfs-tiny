# SPDX-License-Identifier: GPL-3.0-or-later

list(APPEND TEST_SOURCES
	test_base16.cpp
	test_base58.cpp
	test_cid.cpp
	test_hash.cpp
	test_multibase.cpp
	test_multihash.cpp
	test_varint.cpp
	test_x86.cpp
)

add_executable(test-x86 ${TEST_SOURCES})

target_include_directories(test-x86 PUBLIC
	${CPPUNIT_INCLUDE_DIRS}
	"${PROJECT_BINARY_DIR}/etl/include"
	"${PROJECT_SOURCE_DIR}/etl/include"
	"${PROJECT_BINARY_DIR}/include"
	"${PROJECT_SOURCE_DIR}/include"
)

target_link_libraries(test-x86 
    ${CPPUNIT_LIBRARY}
    ipfs-tiny
)

add_test(test-x86 test-x86)
set_tests_properties(test-x86 PROPERTIES TIMEOUT 120)
