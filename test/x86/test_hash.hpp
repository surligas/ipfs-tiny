/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_HASH_HPP_
#define TEST_X86_TEST_HASH_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_hash : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(test_hash);
	CPPUNIT_TEST(test_sha256);
	CPPUNIT_TEST(test_sha256_vectors);
	CPPUNIT_TEST_SUITE_END();

public:
	void
	test_sha256();

	void
	test_sha256_vectors();
};




#endif /* TEST_X86_TEST_HASH_HPP_ */
