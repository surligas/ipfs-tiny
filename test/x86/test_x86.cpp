/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>

#include "test_hash.hpp"
#include "test_base58.hpp"
#include "test_varint.hpp"
#include "test_multihash.hpp"
#include "test_base16.hpp"
#include "test_multibase.hpp"
#include "test_cid.h"

CPPUNIT_TEST_SUITE_REGISTRATION(test_hash);
CPPUNIT_TEST_SUITE_REGISTRATION(test_base58);
CPPUNIT_TEST_SUITE_REGISTRATION(test_varint);
CPPUNIT_TEST_SUITE_REGISTRATION(test_multihash);
CPPUNIT_TEST_SUITE_REGISTRATION(test_base16);
CPPUNIT_TEST_SUITE_REGISTRATION(test_multibase);
CPPUNIT_TEST_SUITE_REGISTRATION(test_cid);

int
main(int argc, char **argv)
{
	CppUnit::TestResult controller;
	CppUnit::TestResultCollector result;
	CppUnit::BriefTestProgressListener progressListener;
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry &registry =
	        CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest());
	runner.eventManager().addListener(&result);
	runner.eventManager().addListener(&progressListener);
	bool success = runner.run("", false);
	return success ? 0 : 1;

}
