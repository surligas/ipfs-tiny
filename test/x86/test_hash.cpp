/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <random>

#include "ipfs-tiny/crypto/sha256.hpp"
#include "test_hash.hpp"

void
test_hash::test_sha256()
{

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<uint8_t> uni(0, 255);
	etl::string < 256 / 8 * 2 > dh0("");
	etl::string < 256 / 8 * 2 > dh1("");

	etl::vector < uint8_t, 256 / 8 > d0;
	etl::vector < uint8_t, 256 / 8 > d1;

	etl::vector<uint8_t, 1024> rv;
	for (size_t i = 0; i < 1024; i++) {
		rv.push_back(uni(mt));
	}

	ipfs_tiny::crypto::sha256 h0 =
	        ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();
	ipfs_tiny::crypto::sha256 h1 =
	        ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();

	CPPUNIT_ASSERT(h0.size() == 256);
	CPPUNIT_ASSERT(h0.fn() == 0x12);

	d0.clear();
	d1.clear();
	h0.digest(d0, rv);
	h1.digest(d1, rv);
	CPPUNIT_ASSERT(d0 == d1);
}

void
test_hash::test_sha256_vectors()
{
	/* NIST test vector 512 bits long */
	const std::string m =
	        "5a86b737eaea8ee976a0a24da63e7ed7eefad18a101c1211e2b3650c5187c2a8a650547208251f6d4237e661c7bf4c77f335390394c37fa1a9f9be836ac28509";
	const std::string res =
	        "42e61e174fbb3897d6dd6cef3dd2802fe67b331953b06114a65c772859dfc1aa";
	etl::vector<uint8_t, 512> m_v;
	etl::vector<uint8_t, 512> res_v;
	etl::vector<uint8_t, 512> orig_res_v;

	for (size_t i = 0; i < 512 / 8 * 2; i += 2) {
		std::string s = "0x" + m.substr(i, 2);
		m_v.push_back(std::stoul(s, nullptr, 0));
	}

	for (size_t i = 0; i < res.size(); i += 2) {
		std::string s = "0x" + res.substr(i, 2);
		orig_res_v.push_back(std::stoul(s, nullptr, 0));
	}

	ipfs_tiny::crypto::sha256 h =
	        ipfs_tiny::crypto::sha256<etl::ivector<uint8_t>>();
	h.digest(res_v, m_v);
	CPPUNIT_ASSERT(res_v == orig_res_v);
}

