/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <random>

#include "ipfs-tiny/multiformats.hpp"
#include "test_varint.hpp"


void
random(size_t bits)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	const uint64_t m = (1UL << bits) - 1;
	std::uniform_int_distribution<uint64_t> uni(0, m);

	/* Test with etl::vector::append() */
	for (size_t i = 0; i < 10000; i++) {
		etl::vector<uint8_t, 9> v;
		uint64_t x = uni(mt);
		ipfs_tiny::multiformats::varint::encode(v, x);
		uint64_t res;
		auto iter = ipfs_tiny::multiformats::varint::decode(res, v.cbegin(), v.cend());
		CPPUNIT_ASSERT(x == res);
		CPPUNIT_ASSERT(iter != v.cbegin());
	}

}

void
test_varint::random_63()
{
	random(63);
}

void
test_varint::random_47()
{
	random(47);
}

void
test_varint::random_31()
{
	random(31);
}

void
test_varint::random_23()
{
	random(23);
}

void
test_varint::random_15()
{
	random(15);
}

void
test_varint::random_7()
{
	random(7);
}
