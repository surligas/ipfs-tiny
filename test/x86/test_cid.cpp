/* SPDX-License-Identifier: GPL-3.0-or-later */#include <random>

#include "test_cid.h"
#include "ipfs-tiny/cid.hpp"
#include <random>
#include "ipfs-tiny/crypto/sha256.hpp"

using namespace ipfs_tiny;


void
test_cid::basic_encoding()
{
	etl::string<50> msg =
	        etl::make_string("QmY7Yh4UquoXHLPFo2XbhXkhBvFoPwmQUSa92pxnxjQuPU");
	etl::string<50> msg2 =
	        etl::make_string("QmY7Yh4UquoXHLPFo2XbhXkhBvFoPwmQUSa92pxnxjQuPX");
	cid<256> x(msg);
	cid<256> y(msg);
	cid<256> z(msg2);
	cid<256> k(msg.begin(), msg.end());
	CPPUNIT_ASSERT(x.version() == cid_version::v0);
	CPPUNIT_ASSERT(y.version() == cid_version::v0);
	CPPUNIT_ASSERT(z.version() == cid_version::v0);
	CPPUNIT_ASSERT(x == y);
	CPPUNIT_ASSERT(k == z);
	//FIXME
	//CPPUNIT_ASSERT(y != z);
}
