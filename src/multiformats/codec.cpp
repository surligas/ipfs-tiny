/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "ipfs-tiny/multiformats/codec.hpp"

namespace ipfs_tiny
{

namespace multiformats
{

codec::codec(const etl::istring &name, uint32_t code)
	: m_name(name),
	  m_code(code)
{
}

uint32_t
codec::code() const
{
	return m_code;
}

const etl::istring &
codec::name() const
{
	return m_name;
}

}  // namespace multiformats

}  // namespace ipfs_tiny

