/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CRYPTO_SHA256_HPP_
#define INCLUDE_IPFS_TINY_CRYPTO_SHA256_HPP_

#include <cstdint>
#include "ipfs-tiny/crypto/hash.hpp"

namespace ipfs_tiny
{

namespace crypto
{

template <typename T>
class sha256 : public hash<T>
{
public:
	static constexpr size_t s_block_size = (512 / 8);

	static const uint32_t K[s_block_size];

	sha256() :
		hash<T> (etl::make_string("sha2-256"), 0x12, 256),
		m_tot_len(0),
		m_finalized(false)
	{
		reset();
	}

	~sha256()
	{

	}

	T &
	digest(T &res);

	T &
	digest(T &res, const T &data);

	T &
	digest(T &res, typename T::const_iterator begin,
	       typename T::const_iterator end);

	void
	update(const T &data);

	void
	update(typename T::const_iterator begin,
	       typename T::const_iterator end);

	void
	reset()
	{
		m_tot_len = 0;
		m_finalized = false;
		m_block.clear();
		m_h[0] = 0x6a09e667;
		m_h[1] = 0xbb67ae85;
		m_h[2] = 0x3c6ef372;
		m_h[3] = 0xa54ff53a;
		m_h[4] = 0x510e527f;
		m_h[5] = 0x9b05688c;
		m_h[6] = 0x1f83d9ab;
		m_h[7] = 0x5be0cd19;
	}

private:
	// From a Twofish code (modified)
	// Bitwise rotation to the right
	template <typename X>
	static X
	ROR(X x, const uint64_t &n, const uint64_t &bits)
	{
		static_assert(std::is_integral <X>::value,
		              "Error: Value being rotated should be integral.");
		return (x >> n) | ((x & ((1ULL << n) - 1)) << (bits - n));
	}

	// Rotate Left
	// Bitwise rotation to the left
	template <typename X>
	static X
	ROL(const X &x, const uint64_t &n, const uint64_t &bits)
	{
		static_assert(std::is_integral <X>::value,
		              "Error: Value being rotated should be integral.");
		return ROR(x, bits - n, bits);
	}


	static uint64_t
	ch(const uint64_t   &m, const uint64_t &n, const uint64_t &o)
	{
		return (m & n) ^ (~m & o);
	}

	static uint64_t
	maj(const uint64_t &m, const uint64_t &n, const uint64_t &o)
	{
		return (m & n) ^ (m & o) ^ (n & o);
	}

	size_t m_tot_len;
	bool m_finalized;
	etl::vector<uint8_t, 2 * s_block_size> m_block;
	uint32_t m_h[s_block_size / 8];


	uint32_t
	S0(const uint32_t &value) const
	{
		return ROR(value, 2, 32) ^ ROR(value, 13, 32) ^ ROR(value,
		                22, 32);
	}
	uint32_t
	S1(const uint32_t &value) const
	{
		return ROR(value, 6, 32) ^ ROR(value, 11, 32) ^ ROR(value,
		                25, 32);
	}

	uint32_t
	s0(const uint32_t &value) const
	{
		return ROR(value, 7, 32) ^ ROR(value, 18, 32) ^ (value >> 3);
	}

	uint32_t
	s1(const uint32_t &value) const
	{
		return ROR(value, 17, 32) ^ ROR(value, 19, 32) ^ (value >> 10);
	}

	void
	calc(const etl::ivector<uint8_t>::const_iterator first,
	     const etl::ivector<uint8_t>::const_iterator last)
	{
		const size_t size = last - first;
		for (size_t n = 0; n < (size >> 6); n++) {
			etl::ivector<uint8_t>::const_iterator it = first + (n << 6);
			uint32_t skey[64];
			for (uint8_t x = 0; x < 16; x++) {
				etl::ivector<uint8_t>::const_iterator tmp_it = it + (x << 2);
				skey[x] = ((*tmp_it) << 24) | ((*(tmp_it + 1)) << 16)
				          | ((*(tmp_it + 2)) << 8) | (*(tmp_it + 3));
			}
			for (uint8_t x = 16; x < 64; x++) {
				skey[x] = s1(skey[x - 2]) + skey[x - 7] + s0(skey[x - 15]) + skey[x - 16];
			}
			uint32_t a = m_h[0];
			uint32_t b = m_h[1];
			uint32_t c = m_h[2];
			uint32_t d = m_h[3];
			uint32_t e = m_h[4];
			uint32_t f = m_h[5];
			uint32_t g = m_h[6];
			uint32_t h = m_h[7];

			for (uint8_t x = 0; x < 64; x++) {
				uint32_t t1 = h + S1(e) + ch(e, f, g) + K[x] + skey[x];
				uint32_t t2 = S0(a) + maj(a, b, c);
				h = g;
				g = f;
				f = e;
				e = d + t1;
				d = c;
				c = b;
				b = a;
				a = t1 + t2;
			}
			m_h[0] += a;
			m_h[1] += b;
			m_h[2] += c;
			m_h[3] += d;
			m_h[4] += e;
			m_h[5] += f;
			m_h[6] += g;
			m_h[7] += h;
		}
		m_tot_len += size;
	}

	void
	finalize()
	{
		/*
		 * If the finalization is already done, skip this so users can get again
		 * the digest
		 */
		if (m_finalized) {
			return;
		}
		size_t block_nb = (1
		                   + ((s_block_size - 9) < (m_block.size() % s_block_size)));

		size_t len_b = (m_tot_len + m_block.size()) * 8;
		size_t pm_len = block_nb * s_block_size;

		m_block.push_back(0x80);
		m_block.resize(pm_len, 0);
		m_block[pm_len - 4] = len_b >> 24;
		m_block[pm_len - 3] = len_b >> 16;
		m_block[pm_len - 2] = len_b >> 8;
		m_block[pm_len - 1] = len_b;
		calc(m_block.begin(), m_block.end());
		m_finalized = true;
	}

};

}  // namespace crypto

}  // namespace ipfs_tiny


#endif /* INCLUDE_IPFS_TINY_CRYPTO_SHA256_HPP_ */
