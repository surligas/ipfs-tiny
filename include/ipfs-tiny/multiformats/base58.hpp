/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP
#define INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP

#include "base.hpp"
#include <cstddef>
#include "etl/format_spec.h"
#include <etl/vector.h>
#include <etl/string.h>

namespace ipfs_tiny
{

namespace multiformats
{

static const char b58digits_ordered[] =
        "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
static const int8_t b58digits_map[] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7,
	        8, -1, -1, -1, -1, -1, -1, -1, 9, 10, 11, 12, 13, 14, 15, 16, -1, 17, 18,
	        19, 20, 21, -1, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, -1, -1, -1, -1,
	        -1, -1, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, -1, 44, 45, 46, 47, 48,
	        49, 50, 51, 52, 53, 54, 55, 56, 57, -1, -1, -1, -1, -1
        };


/**
 * base58 (multibase code: f) class.
 *
 * @tparam T1 input data datatype
 * @tparam T2 output data datatype
 */
template <typename T1, typename T2>
class base58 : public base<T1, T2>
{
public:
	base58() :
		base<T1, T2>(etl::make_string("base58btc"), 'z')
	{}

	etl::ivector<uint8_t> &
	encode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
	{
		size_t previous_size = res.size();
		size_t datasz = data.size();
		size_t b58sz = res.max_size();
		int carry;
		ssize_t i, j, high, zcount = 0;
		size_t size;
		size_t res_size = 0;

		while (zcount < (ssize_t)datasz && !data[zcount])
			++zcount;

		size = base58::encode_size(datasz - zcount);

		res.resize(previous_size + size);

		for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
			for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
				carry += 256 * res[previous_size + j];
				res[previous_size + j] = carry % 58;
				carry /= 58;
			}
		}

		for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
			;

		if (b58sz <= zcount + size - j) {
			b58sz = zcount + size - j + 1;
			return res;
		}

		for (i = zcount; j < (ssize_t)size; ++i, ++j) {
			res_size++;
			res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
		}
		res.resize(previous_size + res_size);

		return res;
	}

	etl::ivector<uint8_t> &
	encode(etl::ivector<uint8_t> &res, etl::ivector<uint8_t>::const_iterator begin,
	       etl::ivector<uint8_t>::const_iterator end)
	{
		size_t previous_size = res.size();
		size_t datasz = end - begin;
		size_t b58sz = res.max_size();
		int carry;
		ssize_t i, j, high, zcount = 0;
		size_t size;
		size_t res_size = 0;

		while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
			++zcount;

		size = base58::encode_size(datasz - zcount);

		res.resize(previous_size + size);

		for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
			for (carry = *(begin + i), j = size - 1; (j > high) || carry; --j) {
				carry += 256 * res[previous_size + j];
				res[previous_size + j] = carry % 58;
				carry /= 58;
			}
		}

		for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
			;

		if (b58sz <= zcount + size - j) {
			b58sz = zcount + size - j + 1;
			return res;
		}

		for (i = zcount; j < (ssize_t)size; ++i, ++j) {
			res_size++;
			res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
		}
		res.resize(previous_size + res_size);

		return res;
	}


	etl::istring &
	encode(etl::istring &res, const etl::ivector<uint8_t> &data)
	{
		size_t previous_size = res.size();
		size_t datasz = data.size();
		size_t b58sz = res.max_size();
		int carry;
		ssize_t i, j, high, zcount = 0;
		size_t size;
		size_t res_size = 0;

		while (zcount < (ssize_t)datasz && !data[zcount])
			++zcount;

		size = base58::encode_size(datasz - zcount);

		res.resize(previous_size + size);

		for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
			for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
				carry += 256 * res[previous_size + j];
				res[previous_size + j] = carry % 58;
				carry /= 58;
			}
		}

		for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
			;

		if (b58sz <= zcount + size - j) {
			b58sz = zcount + size - j + 1;
			return res;
		}

		for (i = zcount; j < (ssize_t)size; ++i, ++j) {
			res_size++;
			res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
		}
		res.resize(previous_size + res_size);

		return res;
	}

	etl::istring &
	encode(etl::istring &res, etl::ivector<uint8_t>::const_iterator begin,
	       etl::ivector<uint8_t>::const_iterator end)
	{
		size_t previous_size = res.size();
		size_t datasz = end - begin;
		size_t b58sz = res.max_size();
		int carry;
		ssize_t i, j, high, zcount = 0;
		size_t size;
		size_t res_size = 0;

		while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
			++zcount;

		size = base58::encode_size(datasz - zcount);

		res.resize(previous_size + size);

		for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
			for (carry = *(begin + i), j = size - 1; (j > high) || carry; --j) {
				carry += 256 * res[previous_size + j];
				res[previous_size + j] = carry % 58;
				carry /= 58;
			}
		}

		for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
			;

		if (b58sz <= zcount + size - j) {
			b58sz = zcount + size - j + 1;
			return res;
		}

		for (i = zcount; j < (ssize_t)size; ++i, ++j) {
			res_size++;
			res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
		}
		res.resize(previous_size + res_size);

		return res;
	}


	etl::ivector<uint8_t> &
	decode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
	{
		size_t len = data.size();
		size_t previous_size = res.size();
		res.push_back(0);
		int resultlen = 1;
		for (int i = 0; i < len; i++) {
			uint16_t carry = (uint16_t)b58digits_map[data[i]];
			for (int j = 0; j < resultlen; j++) {
				carry += (uint16_t)(res[previous_size + j]) * 58;
				res[previous_size + j] = (uint8_t)(carry & 0xff);
				carry >>= 8;
			}
			while (carry > 0) {
				res.push_back((uint16_t)(carry & 0xff));
				resultlen++;
				carry >>= 8;
			}
		}

		for (int i = 0; i < len && data[i] == '1'; i++) {
			res.push_back(0);
			resultlen++;
		}

		for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
		     i >= z; i--) {
			int k = res[previous_size + i];
			res[previous_size + i] = res[previous_size + resultlen - i - 1];
			res[previous_size + resultlen - i - 1] = k;
		}
		return res;
	}

	etl::ivector<uint8_t> &
	decode(etl::ivector<uint8_t> &res, const etl::istring &data)
	{
		size_t len = data.size();
		size_t previous_size = res.size();
		res.push_back(0);
		int resultlen = 1;
		for (int i = 0; i < len; i++) {
			uint16_t carry = (uint16_t)b58digits_map[data[i]];
			for (int j = 0; j < resultlen; j++) {
				carry += (uint16_t)(res[previous_size + j]) * 58;
				res[previous_size + j] = (uint8_t)(carry & 0xff);
				carry >>= 8;
			}
			while (carry > 0) {
				res.push_back((uint16_t)(carry & 0xff));
				resultlen++;
				carry >>= 8;
			}
		}

		for (int i = 0; i < len && data[i] == '1'; i++) {
			res.push_back(0);
			resultlen++;
		}

		for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
		     i >= z; i--) {
			int k = res[previous_size + i];
			res[previous_size + i] = res[previous_size + resultlen - i - 1];
			res[previous_size + resultlen - i - 1] = k;
		}
		return res;
	}


	/**
	 * Decodes a range of data in the range of [\p begin, \p end)
	 * @param res the structure to append the result
	 * @param begin iterator to the first input data
	 * @param end iterator to the last input data
	 * @return reference to \p res
	 */
	etl::ivector<uint8_t> &
	decode(etl::ivector<uint8_t> &res, etl::istring::const_iterator begin,
	       etl::istring::const_iterator end)
	{
		size_t len = end - begin;
		size_t previous_size = res.size();
		res.push_back(0);
		int resultlen = 1;
		for (int i = 0; i < len; i++) {
			uint16_t carry = (uint16_t)b58digits_map[*(begin + i)];
			for (int j = 0; j < resultlen; j++) {
				carry += (uint16_t)(res[previous_size + j]) * 58;
				res[previous_size + j] = (uint8_t)(carry & 0xff);
				carry >>= 8;
			}
			while (carry > 0) {
				res.push_back((uint16_t)(carry & 0xff));
				resultlen++;
				carry >>= 8;
			}
		}

		for (int i = 0; i < len && (*(begin + i) == '1'); i++) {
			res.push_back(0);
			resultlen++;
		}

		for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
		     i >= z; i--) {
			int k = res[previous_size + i];
			res[previous_size + i] = res[previous_size + resultlen - i - 1];
			res[previous_size + resultlen - i - 1] = k;
		}
		return res;
	}

	/**
	 * Decodes a range of data in the range of [\p begin, \p end)
	 * @param res the structure to append the result
	 * @param begin iterator to the first input data
	 * @param end iterator to the last input data
	 * @return reference to \p res
	 */
	etl::ivector<uint8_t> &
	decode(etl::ivector<uint8_t> &res, etl::ivector<uint8_t>::const_iterator begin,
	       etl::ivector<uint8_t>::const_iterator end)
	{
		size_t len = end - begin;
		size_t previous_size = res.size();
		res.push_back(0);
		int resultlen = 1;
		for (int i = 0; i < len; i++) {
			uint16_t carry = (uint16_t)b58digits_map[*(begin + i)];
			for (int j = 0; j < resultlen; j++) {
				carry += (uint16_t)(res[previous_size + j]) * 58;
				res[previous_size + j] = (uint8_t)(carry & 0xff);
				carry >>= 8;
			}
			while (carry > 0) {
				res.push_back((uint16_t)(carry & 0xff));
				resultlen++;
				carry >>= 8;
			}
		}

		for (int i = 0; i < len && (*(begin + i) == '1'); i++) {
			res.push_back(0);
			resultlen++;
		}

		for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
		     i >= z; i--) {
			int k = res[previous_size + i];
			res[previous_size + i] = res[previous_size + resultlen - i - 1];
			res[previous_size + resultlen - i - 1] = k;
		}
		return res;
	}

	size_t
	encode_size(size_t input_size) const
	{
		return (input_size) * 138 / 100 + 1;
	}
	size_t
	decode_size(size_t input_size) const
	{
		return (input_size) * 100 / 138 + 1;
	}
};

} // namespace multiformats

} // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP */
