/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_

#include <etl/string.h>
#include <etl/map.h>
#include "codec.hpp"
#include "ipfs-tiny/exception.hpp"

namespace ipfs_tiny
{

namespace multiformats
{

class multicodec_uknown_codec : public exception
{
public:
	multicodec_uknown_codec(const char *file, size_t line) :
		exception("multicodec: Unknown codec", file, line)
	{
	}

};

class multicodec
{
public:
	enum code : uint32_t {
		identity = 0x0, dag_pb = 0x70
	};

	multicodec() :
		m_codecs {
		{ 0x0, codec(etl::make_string("identity"), 0x0) },
		{ 0x70, codec(etl::make_string("dag-pb"), 0x70) } },
	m_code(0)
	{
	}

	multicodec(code c) :
		m_codecs {
		{ 0x0, codec(etl::make_string("identity"), 0x0) },
		{ 0x70, codec(etl::make_string("dag-pb"), 0x70) } },
	m_code(c)
	{
	}

	template<typename InputIterator>
	InputIterator
	decode(InputIterator first)
	{
		uint64_t res;
		first = varint(res, first, first);
		try {
			m_codecs.at(res);
			m_code = res;
		} catch (etl::map_out_of_bounds &e) {
			throw multicodec_uknown_codec(__FILE__, __LINE__);
		}
		return first;
	}

private:
	const etl::map<uint64_t, const codec, 2> m_codecs;
	uint64_t m_code;
};

}  // namespace multiformats

}  // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_ */
