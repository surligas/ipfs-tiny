/*
 * multibase.hpp
 *
 *  Created on: Jun 10, 2021
 *      Author: surligas
 */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_

#include "base.hpp"
#include "base16.hpp"
#include "base58.hpp"
#include "ipfs-tiny/exception.hpp"
#include "etl/map.h"
#include <type_traits>

namespace ipfs_tiny
{

namespace multiformats
{

class multibase_uknown_base : public exception
{
public:
	multibase_uknown_base(const char *file, size_t line) :
		exception("multibase: Unknown algorithm", file, line) {}
};


namespace multibase
{
/**
 *
 */
enum class encoding : char {
	extract = 0,   /**< Search for the encoding. Valid only during decoding */
	base16 = 'f',  /**< base16 */
	base58btc = 'z'/**< base58btc */
};


template<encoding T, typename EncodedT, typename DecodedT>
static EncodedT &
encode(EncodedT &res, const DecodedT &data)
{
	if constexpr(T == encoding::base16) {
		base16<DecodedT, EncodedT> b16;
		etl::string<1> c;
		c[0] = b16.code();
		res.append(c);
		return b16.encode(res, data);
	} else if constexpr(T == encoding::base58btc) {
		base58<DecodedT, EncodedT> b58;
		etl::string<1> c;
		c[0] = b58.code();
		res.append(c);
		return b58.encode(res, data);
	} else {
		throw exception("multibase: Unsupported encoding algorithm",
		                __FILE__, __LINE__);
	}
}


};

}  // namespace multiformats

}  // namespace ipfs_tiny

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_ */
